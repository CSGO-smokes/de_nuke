# de_nuke smokes

## smoke for awkward spot above hut

![above_hut_01.jpg](img/above_hut_01.jpg)

![above_hut_02.jpg](img/above_hut_02.jpg)

![above_hut_03_result.jpg](img/above_hut_03_result.jpg)


## Smoke for gap between big garage and white container

![awp_container_01.jpg](img/awp_container_01.jpg)

![awp_container_02.jpg](img/awp_container_02.jpg)

![awp_container_03.jpg](img/awp_container_03.jpg)

![awp_container_04_result.jpg](img/awp_container_04_result.jpg)


## Smoke big garage outside

![big_garage_01.jpg](img/big_garage_01.jpg)

![big_garage_02.jpg](img/big_garage_02.jpg)

![big_garage_03.jpg](img/big_garage_03.jpg)

![big_garage_04_result.jpg](img/big_garage_04_result.jpg)

## Alternative big garage smoke outside

![big_garage_alt_01.jpg](img/big_garage_alt_01.jpg)

![big_garage_alt_02.jpg](img/big_garage_alt_02.jpg)

![big_garage_alt_03_result.jpg](img/big_garage_alt_03_result.jpg)


## Smoke heaven from T roof

Remember to pop the windows like so:

![heaven_01.jpg](img/heaven_01.jpg)

![heaven_02.jpg](img/heaven_02.jpg)

![heaven_03.jpg](img/heaven_03.jpg)

![heaven_04_result.jpg](img/heaven_04_result.jpg)


## Smoke secret cross

![secret_alt_01.jpg](img/secret_alt_01.jpg)

![secret_alt_02.jpg](img/secret_alt_02.jpg)

![secret_alt_03_result.jpg](img/secret_alt_03_result.jpg)


## Smoke secret cross, o smoke

![secret_o_smoke_01.jpg](img/secret_o_smoke_01.jpg)

![secret_o_smoke_02.jpg](img/secret_o_smoke_02.jpg)

![secret_o_smoke_03_result.jpg](img/secret_o_smoke_03_result.jpg)

## Smoke vent from T roof for push

### REMEMBER TO POP WINDOWS

![vent_01.jpg](img/vent_01.jpg)

![vent_02.jpg](img/vent_02.jpg)

![vent_03_result.jpg](img/vent_03_result.jpg)


